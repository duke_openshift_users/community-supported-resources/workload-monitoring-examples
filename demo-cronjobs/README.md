What is this?

This is a set of configurations to test user monitoring of CronJob resources.

We have 3 example CronJobs. `winny`, `failey`, and `slowy` which will suceed, fail, and exceed a configured timeout respectively. Then there are the Prometheus and Alertmanager CRDs which configure rules to monitor those 3 cron jobs, define a notification profile (an email in this case) and then connect the rules to the notification profile.
