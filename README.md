This repo holds some examples of how one could monitor their own pods inside an okd4 cluster with user-managed workload monitoring enabled.

Some Upstream Docs:

[For Metrics](https://docs.okd.io/latest/monitoring/managing-metrics.html#understanding-metrics_managing-metrics)

[For Alerts](https://docs.okd.io/latest/monitoring/managing-alerts.html#managing-alerting-rules-for-user-defined-projects_managing-alerts)